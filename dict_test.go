package dict

import (
	"testing"
)

func TestDict(t *testing.T) {
	dict := New[int, string]()
	dict.Set(1, "hello")
	x, found := dict.Get(1)
	if x != "hello" {
		t.Fatalf("x must be hello")
	}
	if !found {
		t.Fatalf("must found key 1")
	}
}
